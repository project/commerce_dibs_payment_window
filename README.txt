Commerce DIBS (Mobile) Payment Window

-- REQUIREMENTS --

Access to DIBS payment gateway

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:
* Configure payment details in Store » Configuration » Payment methods.

