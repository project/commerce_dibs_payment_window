<?php

/**
 * @file
 * Admin page callbacks.
 */

/**
 * Page callback: Called by DIBS when an customer pays.
 */
function commerce_dibs_payment_window_callback($order, $transaction) {

  // Store each callback request attempt to log.
  watchdog('commerce_dibs_payment_window', 'Callback: <pre>%pp</pre><pre>%op</pre><pre>%tp</pre>',
    array(
      '%pp' => print_r($_POST, TRUE),
      '%op' => print_r($order, TRUE),
      '%tp' => print_r($transaction, TRUE),
    )
  );

  try {

    if (empty($_POST)) {
      throw new Exception('DIBS Callback, no data provided from DIBS.');
    }
    $response = $_POST;

    // Save response data for later.
    $transaction->data['commerce_dibs_payment_window']['responses'][REQUEST_TIME] = $response;

    // Load inc.
    module_load_include('inc', 'commerce_dibs_payment_window', 'commerce_dibs_payment_window');

    // Load payment method instance.
    $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
    if (empty($payment_method) || $payment_method['method_id'] !== 'commerce_dibs_payment_window') {
      throw new Exception('DIBS Callback, failed to load payment method. Aborted.');
    }

    // Load settings with default settings fallback.
    $settings = array_replace_recursive(_commerce_dibs_payment_window_get_settings_default(), $payment_method['settings']);

    // Skip next steps if transaction has been processed by form_validation
    // already.
    if (isset($response['captureStatus']) && $transaction->remote_status === COMMERCE_DIBS_PAYMENT_WINDOW_REMOTE_STATUS_CAPTURED && $transaction->status != COMMERCE_DIBS_PAYMENT_WINDOW_STATUS_UNDEFINED) {
      return MENU_NOT_FOUND;
    }
    elseif (!isset($response['captureStatus']) && $transaction->remote_status === COMMERCE_DIBS_PAYMENT_WINDOW_REMOTE_STATUS_AUTHORIZED && $transaction->status != COMMERCE_DIBS_PAYMENT_WINDOW_STATUS_UNDEFINED) {
      return MENU_NOT_FOUND;
    }

    // Check for MAC from DIBS.
    $client = _commerce_dibs_payment_window_get_client($payment_method);
    $client->verifyMacResponse($response);
  }
  catch (Exception $e) {
    watchdog_exception('commerce_dibs_payment_window', $e);
    return MENU_NOT_FOUND;
  }

  try {
    $checks = array(
      's_localTransactionID' => $transaction->transaction_id,
      'orderId' => $order->order_id,
      'merchant' => $settings['commerce_dibs_payment_window_merchant_id'],
      'transaction' => NULL,
      'amount' => $transaction->amount,
    );
    _commerce_dibs_payment_window_validate_response($response, $checks);

    // Default statuses.
    $transaction->remote_id = check_plain($response['transaction']);
    $transaction->remote_status = COMMERCE_DIBS_PAYMENT_WINDOW_REMOTE_STATUS_AUTHORIZED;
    $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
    $transaction->log = $transaction->message = 'Transaction authorized via callback';

    // Check for instant capture. ERROR or DECLINED should throw an Exception.
    if (isset($response['captureStatus']) && in_array($response['captureStatus'], array('ERROR', 'DECLINED'))) {
      throw new Exception(format_string(
        'DIBS Callback, failed to capture funds for order: %order_id and transaction: %transaction_id. Please consult your DIBS administration.',
        array(
          '%order_id' => $order->order_id,
          '%transaction_id' => $transaction->transaction_id,
        )
      ));
    }
    elseif (isset($response['captureStatus']) && $response['captureStatus'] == 'ACCEPTED') {
      $transaction->remote_status = COMMERCE_DIBS_PAYMENT_WINDOW_REMOTE_STATUS_CAPTURED;
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->log = $transaction->message = 'Transaction captured via callback';
    }

    $transaction->remote_id = check_plain($response['transaction']);

    // Save response as a payload.
    $transaction->data['last_payload'] = $transaction->payload = $response;

    if (!empty($transaction->log)) {
      $transaction->revision = TRUE;
    }

    // Save transaction, should return SAVED_UPDATED.
    commerce_payment_transaction_save($transaction);

    return 'OK';
  }
  catch (Exception $e) {
    watchdog_exception('commerce_dibs_payment_window', $e);

    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    $transaction->log = $transaction->message = $e->getMessage();
    $transaction->data['last_payload'] = $transaction->payload = $_POST;
    $transaction->revision = TRUE;
    commerce_payment_transaction_save($transaction);

    return MENU_NOT_FOUND;
  }
}

/**
 * Capture form.
 */
function commerce_dibs_payment_window_admin_capture($form, &$form_state, $transaction, $order) {
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );
  $form['transaction'] = array(
    '#type' => 'value',
    '#value' => $transaction,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => commerce_currency_amount_to_decimal(($transaction->payload['amount'] - $transaction->amount ? $transaction->payload['amount'] - $transaction->amount : $transaction->amount), $transaction->currency_code),
    '#element_validate' => array('element_validate_number'),
  );
  return confirm_form($form, t('Are you sure you want to %action transaction %transaction.', array('%action' => strtolower(t('Capture')), '%transaction' => $transaction->transaction_id)), $uri['path']);
}

/**
 * Capture form: submit handler.
 */
function commerce_dibs_payment_window_admin_capture_submit(&$form, &$form_state) {
  $transaction = $form_state['values']['transaction'];
  $amount = commerce_currency_decimal_to_amount($form_state['values']['amount'], $transaction->currency_code);
  try {
    module_load_include('inc', 'commerce_dibs_payment_window');
    _commerce_dibs_payment_window_transaction_capture($transaction, $amount);
    drupal_set_message(t('Transaction has been successfully %action.', array('%action' => strtolower(t('Captured')))));
    $uri = commerce_payment_ui_payment_transaction_uri($transaction);
    $form_state['redirect'] = $uri['path'];
  }
  catch (Exception $e) {
    drupal_set_message(t('Unable to %action transaction: %message', array('%action' => strtolower(t('Capture')), '%message' => $e->getMessage())), 'error');
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Cancel form.
 */
function commerce_dibs_payment_window_admin_cancel($form, &$form_state, $transaction, $order) {
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );
  $form['transaction'] = array(
    '#type' => 'value',
    '#value' => $transaction,
  );
  return confirm_form($form, t('Are you sure you want to %action transaction %transaction.', array('%action' => strtolower(t('Cancel')), '%transaction' => $transaction->transaction_id)), $uri['path']);
}

/**
 * Cancel form: submit handler.
 */
function commerce_dibs_payment_window_admin_cancel_submit(&$form, &$form_state) {
  $transaction = $form_state['values']['transaction'];
  try {
    module_load_include('inc', 'commerce_dibs_payment_window');
    _commerce_dibs_payment_window_transaction_cancel($transaction);
    drupal_set_message(t('Transaction has been successfully %action.', array('%action' => strtolower(t('Canceled')))));
    $uri = commerce_payment_ui_payment_transaction_uri($transaction);
    $form_state['redirect'] = $uri['path'];
  }
  catch (Exception $e) {
    drupal_set_message(t('Unable to %action transaction: %message', array('%action' => strtolower(t('Cancel')), '%message' => $e->getMessage())), 'error');
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Credit form.
 */
function commerce_dibs_payment_window_admin_credit($form, &$form_state, $transaction, $order) {
  $uri = commerce_payment_ui_payment_transaction_uri($transaction);
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );
  $form['transaction'] = array(
    '#type' => 'value',
    '#value' => $transaction,
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => commerce_currency_amount_to_decimal($transaction->amount, $transaction->currency_code),
    '#element_validate' => array('element_validate_number'),
  );
  return confirm_form($form, t('Are you sure you want to %action transaction %transaction.', array('%action' => strtolower(t('Credit')), '%transaction' => $transaction->transaction_id)), $uri['path']);
}

/**
 * Credit form: submit handler.
 */
function commerce_dibs_payment_window_admin_credit_submit(&$form, &$form_state) {
  $transaction = $form_state['values']['transaction'];
  $amount = commerce_currency_decimal_to_amount($form_state['values']['amount'], $transaction->currency_code);
  try {
    module_load_include('inc', 'commerce_dibs_payment_window');
    _commerce_dibs_payment_window_transaction_credit($transaction, $amount);
    drupal_set_message(t('Transaction has been successfully %action.', array('%action' => strtolower(t('Creditd')))));
    $uri = commerce_payment_ui_payment_transaction_uri($transaction);
    $form_state['redirect'] = $uri['path'];
  }
  catch (Exception $e) {
    drupal_set_message(t('Unable to %action transaction: %message', array('%action' => strtolower(t('Credit')), '%message' => $e->getMessage())), 'error');
    $form_state['rebuild'] = TRUE;
  }
}
